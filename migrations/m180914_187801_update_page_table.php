<?php

use yii\db\Migration;

/**
 * @since 1.0.0
 */
class m180914_187801_update_page_table extends Migration
{
    /**
     * @var string
     */
    private $_tableName;
    
    public function init()
    {
        parent::init();
        $this->_tableName = Yii::$app->getModule('page-manager')->tableName;
    }
    
    public function up()
    {
        $this->addColumn($this->_tableName, 'index', 'integer(7) not null default 0');
    }

    public function down()
    {
        $this->dropColumn($this->_tableName, 'index');
    }
}
