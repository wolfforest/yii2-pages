<?php

use yii\db\Migration;

/**
 * @since 1.0.0
 */
class m180914_113001_update_page_table extends Migration
{
    /**
     * @var string
     */
    private $_tableName;
    
    public function init()
    {
        parent::init();
        $this->_tableName = Yii::$app->getModule('page-manager')->tableName;
    }
    
    public function up()
    {
        $this->addColumn($this->_tableName, 'parent', 'integer default null');
        $this->addColumn($this->_tableName, 'menutitle', 'varchar(32) not null');
    }

    public function down()
    {
        $this->dropColumn($this->_tableName, 'parent');
        $this->dropColumn($this->_tableName, 'menutitle');
    }
}
