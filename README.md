Yii2 page manager
=================
Page manager for yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist forest/yii2-pages "*"
```

or add

```
"forest/yii2-pages": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \forest\pages\AutoloadExample::widget(); ?>```