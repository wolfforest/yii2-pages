<?php

use yii\helpers\Html;
use forest\pages\Module;

/* @var $this yii\web\View */
/* @var $model forest\pages\models\Page */
/* @var $module forest\pages\Module */
/* @var $tree array */

$this->title = Module::t('Create');
$this->params['breadcrumbs'][] = ['label' => Module::t('Page manager'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
    'module' => $module,
    'tree' => $tree,
]); ?>
