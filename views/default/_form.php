<?php

use forest\pages\models\Page;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use forest\pages\Module;
use vova07\imperavi\Widget as Imperavi;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model forest\pages\models\Page */
/* @var $form yii\widgets\ActiveForm */
/* @var $module forest\pages\Module */
/* @var $tree array */



?>
<div class="row">
    <div class="col-md-12">
        <?php Pjax::begin(['id'=>'workspace'])?>
        <h3><?= Module::t('Page: {0}', Html::encode($model->title)) ?></h3>
        <?php
        $form = ActiveForm::begin([
            'options' => [
                //'data-pjax' => 'workspace'
            ]
        ]);
        ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'template')->dropDownList(Module::getTemplateDropDownList()); ?>
                <?= $form->field($model, 'parent')->dropDownList(Page::getParentDropDownList($model)); ?>
                <?= $form->field($model, 'menutitle')->textInput(['maxlength' => 32, 'id'=>'inp-name']); ?>
                <?= $form->field($model, 'index')->textInput(['maxlength' => 7]); ?>
                <?= $form->field($model, 'published')->checkbox(); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'title_browser')->textInput(['maxlength' => 255]); ?>
                <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 200]); ?>
                <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 160]); ?>
                <?= $form->field($model, 'alias')->textInput(['maxlength' => 255, 'data-alias'=>'#inp-name']); ?>
                <?php if(!$model->isNewRecord):?>
                    <?= Module::t('Page URL:') ?> <?= Html::a(Url::to([Module::getInstance()->pageController, 'alias'=>$model->alias]), [Module::getInstance()->pageController, 'alias'=>$model->alias], [
                        'target' => '_blank',
                        'data-pjax' => '0',
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'title')->textInput(['maxlength' => 255]); ?>
                <?= $form->field($model, 'content')->widget(Imperavi::class, [
                    'settings' => [
                        'minHeight' => 200,
                        'plugins' => [
                            'clips',
                            'fullscreen',
                            'imagemanager'
                        ],
                        'imageManagerJson' => Url::to(['images-get']),
                        'imageUpload' => Url::to(['image-upload']),
                        'imageDelete' => Url::to(['file-delete']),
                    ],
                    'plugins' => [
                        'imagemanager' => 'vova07\imperavi\bundles\ImageManagerAsset',
                    ],
                ]) ?>
            </div>
        </div>

        <?= \app\widgets\Alert::widget() ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Module::t('Create') : Module::t('Save'), [
                'class' => $model->isNewRecord ? 'btn btn-white btn-bold btn-round btn-success' : 'btn btn-white btn-bold btn-round btn-primary',
            ]); ?>
            <?php if(!$model->isNewRecord):?>
                <?= Html::a(Module::t('Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-white btn-bold btn-round btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif;?>
        </div>
        <?php ActiveForm::end(); ?>
        <?php Pjax::end()?>

    </div>
</div>