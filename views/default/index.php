<?php

use yii\helpers\Html;
use yii\grid\GridView;
use forest\pages\Module;
use forest\pages\models\Page;

/* @var $this yii\web\View */
/* @var $searchModel bupy7\pages\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('Page manager');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?> <?= Html::a('<i class="ace-icon glyphicon glyphicon-plus"></i>'.Module::t('Add'), ['create'], ['class' => 'btn btn-white btn-default btn-round']); ?></h1>
</div>
<?= GridView::widget([
    'tableOptions' => [
        'class' => 'table table-striped',
    ],
    'options' => [
        'class' => 'table-responsive',
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'contentOptions' => ['style' => 'width:80px;'],
        ],
        [
            'attribute' => 'title',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a(Html::encode($data->title), ['update', 'id'=>$data->id],['data-pjax'=>0]);
            },
        ],
        'alias',
        'created_at:RelativeTime',
        'updated_at:datetime',
        [
            'attribute' => 'published',
            'filter' => Page::publishedDropDownList(),
            'value' => function ($model) {
                return Yii::$app->formatter->asBoolean($model->published);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => "{update}\n{delete}",
        ],
    ],
]); ?>
