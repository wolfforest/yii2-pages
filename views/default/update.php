<?php

use yii\helpers\Html;
use forest\pages\Module;

/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */
/* @var $module bupy7\pages\Module */
/* @var $tree array */

$this->title = Module::t('Update');
$this->params['breadcrumbs'][] = ['label' => Module::t('Page manager'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php \yii\widgets\Pjax::begin(['id'=>'page-form1']) ?>
<?= $this->render('_form', [
    'model' => $model,
    'module' => $module,
    'tree' => $tree,
]); ?>
<?php \yii\widgets\Pjax::end() ?>