<?php

namespace forest\pages\models;

use Yii;
use forest\pages\Module;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $menutitle
 * @property string $parent
 * @property string $template
 * @property string $alias
 * @property integer $published
 * @property string|null $content
 * @property string|null $title_browser
 * @property string|null $meta_keywords
 * @property string|null $meta_description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $index
 *
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class Page extends ActiveRecord
{

    const PUBLISHED_NO = 0;
    const PUBLISHED_YES = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('page-manager')->tableName;
    }

    public static function getParentDropDownList($for_page)
    {
        $pages = self::find()->where('parent is null')->asArray(['id', 'title', 'parent'])->all();
        return [null => '--'] + ArrayHelper::map($pages, 'id', 'menutitle');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','menutitle','alias','index'], 'required'],
            [['published'], 'boolean'],
            [['parent','index'], 'integer'],
            [['content'], 'string', 'max' => 65535],
            [['title', 'alias', 'title_browser'], 'string', 'max' => 255],
            [['meta_keywords'], 'string', 'max' => 200],
            [['meta_description'], 'string', 'max' => 160],
            [['menutitle', 'template'], 'string', 'max' => 32],
            [['alias'], 'unique'],
            [['alias'], 'filter_alias'],
            [['index'], 'default', 'value'=>50, 'on' => 'insert'],
        ];
    }

    public function filter_alias()
    {
        if(!preg_match('/^[A-z0-9_-]+$/', $this->alias)){
            $this->addError('alias', 'Разрешенные символы - A-z0-9_-.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('ID'),
            'parent' => Module::t('Parent page'),
            'title' => Module::t('Title'),
            'menutitle' => Module::t('Menu title'),
            'template' => Module::t('Template'),
            'alias' => Module::t('Alias'),
            'published' => Module::t('Published'),
            'content' => Module::t('Content'),
            'title_browser' => Module::t('Title browser'),
            'meta_keywords' => Module::t('Meta keywords'),
            'meta_description' => Module::t('Meta description'),
            'created_at' => Module::t('Created at'),
            'updated_at' => Module::t('Updated at'),
            'index' => Module::t('Sort'),
        ];
    }
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    public static function publishedDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES),
        ];
    }

    /**
     * @param $alias
     * @return \yii\db\ActiveQuery
     */
    public static function getByAlias($alias)
    {
        return self::find()->where(['alias' => $alias]);
    }
}
