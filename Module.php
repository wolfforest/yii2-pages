<?php

namespace forest\pages;

use Yii;

/**
 * @property array pageTemplates
 */
class Module extends \yii\base\Module
{

    public $tableName = '{{%page}}';
    public $pageController = '/site/page';
    public $pathToImages;
    public $behaviors;
    public $pageTemplates = [
        'page-text' => 'Text page',
    ];

    public static function getTemplateDropDownList()
    {
        return Yii::$app->getModule('page-manager')->pageTemplates;
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function behaviors()
    {
        return $this->behaviors;
    }
    
    /**
     * Registeration translation files.
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['forest/pages/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'forceTranslation' => true,
            'basePath' => '@forest/pages/messages',
            'fileMap' => [
                'forest/pages/core' => 'core.php',
            ],
        ];
    }

    /**
     * Translates a message to the specified language.
     *
     * @param string $message the message to be translated.
     * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
     * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current application language
     * will be used.
     * @return string
     */
    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('forest/pages/core', $message, $params, $language);
    }
}
