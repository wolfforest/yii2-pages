<?php
/**
 * Created by Forest.
 * Date: 13.09.18 17:57
 */
namespace forest\pages\controllers;

use forest\pages\models\Page;
use forest\pages\models\PageSearch;
use forest\pages\Module;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Url::to('/files/imperavi/', true), // Directory URL address, where files are stored.
                'path' => '@webroot/files/imperavi', // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif']], // These options are by default.
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to('/files/imperavi/', true), // Directory URL address, where files are stored.
                'path' => '@webroot/files/imperavi', // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to('/files/imperavi/'), // Directory URL address, where files are stored.
                'path' => '@webroot/files/imperavi', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate()
    {
        return $this->actionUpdate(null);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer|null $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        if ($id === null) {
            $model = new Page();
        } else {
            $model = $this->findModel($id);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Module::t('Save success'));
            if(!Yii::$app->request->isPjax){
                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        $module = Yii::$app->getModule('pages');

        $page_tree = [];
        $pages = Page::find()->where('parent is null')
            ->orderBy('index')
            ->asArray()->select(['id','parent','menutitle as title','published'])->all();
        foreach ($pages as &$page) {
            $page['url_edit'] = Url::to(['default/update', 'id'=>$page['id']]);
            $children = Page::find()->where(['parent' => $page['id']])
                ->orderBy('index')
                ->asArray()->select(['id','parent','menutitle as title','published'])->all();
            if(count($children)){
                $page['folder'] = true;
                $page['expanded'] = true;
                foreach ($children as &$child) {
                    $child['url_edit'] = Url::to(['default/update', 'id'=>$child['id']]);
                    $child['active'] = $model->id === +$child['id'];
                }
                $page['children'] = $children;
            }
            //$page['unselectableStatus'] = $model->id === +$page['id'];
            $page['active'] = $model->id === +$page['id'];
        }

        return $this->render($id === null ? 'create' : 'update', [
            'model' => $model,
            'module' => $module,
            'tree' => $pages,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('success', Module::t('Delete success'));
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Module::t('Page not found'));
    }
}