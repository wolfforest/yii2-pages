<?php

return [
    'Published' => 'Опубликовано',
    'Template' => 'Шаблон',
    'Home' => 'Главная',

    'Page manager' => 'Управление страницами',
    'Add' => 'Добавить',
    'Title' => 'Заголовок',
    'Alias' => 'Алиас',
    'Created at' => 'Создано',
    'Updated at' => 'Изменено',

    'Pages' => 'Страницы',
    'Add page' => 'Добавить страницу',
    'Parent page' => 'Вложено в',
    'Title browser' => 'Заголовок браузера',
    'Menu title' => 'Пунк в меню',
    'Sort' => 'Сортировка',
    'Content' => 'Содержимое',

    'Save' => 'Сохранить',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    'Page URL:' => 'URL страницы:',
    'Page: {0}' => 'Страница: {0}',
    'Create' => 'Создать',
    'Save success' => 'Сохранено',
];