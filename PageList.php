<?php

namespace forest\pages;

use forest\pages\models\Page;
use yii\base\Component;
use yii\helpers\Url;

/**
 * This is just an example.
 */
class PageList extends Component
{
    public static function getMenuItems($parent_id = null, $levels = 1)
    {
        return self::getByParent($parent_id, $levels);
    }

    public static function getByParent($parent_id = null, $levels = 1)
    {
        --$levels;
        $page_controller = \Yii::$app->getModule('page-manager')->pageController;
        $items = [];
        $pages = Page::find()
            ->where($parent_id?['parent'=>$parent_id]:'parent is null')
            ->andWhere('published = 1')
            ->orderBy('index')
            ->select(['id','menutitle','alias'])->asArray()->all();
        foreach ($pages as $page) {
            if($levels) $children = self::getByParent($page['id'], $levels);
            $item = [
                'label' => $page['menutitle'],
                'url' => [$page_controller, 'alias'=>$page['alias']],
                'items' => $children,
            ];
            $items[] = $item;
        }
        return $items;
    }
}
